export default class Clock {
    constructor(font) {
        this.font = font;
        this.hours = 0;
        this.minutes = 0;
    }

    willUpdate(now) {
        const nowHours = now.getHours();
    	const nowMinutes = now.getMinutes();
    	return nowHours != this.hours || nowMinutes != this.minutes;
    }

    update(now, screen, color) {
    	this.hours = now.getHours();
    	this.minutes = now.getMinutes();

    	const hourStr = parseInt(this.hours % 12).toString();
    	const minuteStr = parseInt(this.minutes).toString();

    	const timeString = hourStr.padStart(2, '0')+":"+minuteStr.padStart(2, '0');
    	const textWidth = screen.getTextWidth(timeString, this.font);
    	const x = (screen.width - textWidth) >> 1;
    	screen.drawText(timeString, this.font, color, x, 0);
    }
}
