import Digital from "pins/digital";

const BUTTON_UP = 1;
const BUTTON_DOWN = 0;

export const Mode = {
    Clock: "Clock",
    Timer: "Timer"
}

export default class ModeHandler {
    constructor() {
        this.mode = Mode.Clock;

        this.button = new Digital(0, Digital.InputPullUp);
        this.prevButtonState = BUTTON_UP;
    }

    update() {
        const buttonState = this.button.read();
    	if (buttonState != this.prevButtonState) {
    		if (buttonState == BUTTON_UP) {
    			if (this.mode == Mode.Clock)
                {
                    this.mode = Mode.Timer;
                }
    			else
                {
                    this.mode = Mode.Clock;
                }
    		}
    		this.prevButtonState = buttonState;
    	}

        return this.mode;
    }
}
