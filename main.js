import Timer from "timer";
import parseBMF from "commodetto/parseBMF";
import parseBMP from "commodetto/parseBMP";
import Poco from "commodetto/Poco";
import Resource from "Resource";
import WiFi from "wifi";
import {File, Iterator, System} from "file";
import config from "mc/config";

import ModeHandler, {Mode} from "mode";
import Clock from "clock";
import Weather from "weather";

const render = new Poco(screen, { rotation: 270, displayListLength: 2048 });
const BLACK = render.makeColor(0, 0, 0);
const WHITE = render.makeColor(255, 255, 255);
const TIME_FONT = parseBMF(new Resource("libre_baskerville_bold_32.bf4"));
const WEATHER_FONT = parseBMF(new Resource("libre_baskerville_bold_14.bf4"));

const file = new File(config.file.root + "settings.json");
const settings = JSON.parse(file.read(String));
file.close();

render.begin();
render.fillRectangle(BLACK, 0, 0, render.width, render.height);
render.end();
Timer.delay(100);

render.begin();
render.fillRectangle(WHITE, 0, 0, render.width, render.height);
render.end();
Timer.delay(100);

let wifiConnected = false;
const ssid = settings.ssid;
const password = settings.password;

const wifi = new WiFi(
	{ssid: ssid, password: password},
	(message) => {
		if (message === "gotIP") {
			wifiConnected = true;
		}
		trace("WiFi: " + message + "\n");
	}
);

const modeHandler = new ModeHandler();
const clock = new Clock(TIME_FONT, BLACK);
const weather = new Weather(settings, WEATHER_FONT, BLACK);

Timer.repeat(id => {
	const mode = modeHandler.update();
	const now = new Date(Date.now());

	if (mode == Mode.Clock) {
		let updateClock = clock.willUpdate(now);
		let updateWeather = wifiConnected && weather.willUpdate(now);

		if (updateClock || updateWeather) {
			render.begin();
			render.fillRectangle(BLACK, 0, 0, render.width, render.height);
			clock.update(now, render, WHITE);
			weather.update(now, render, WHITE);
			render.end();
			render.begin();
			render.fillRectangle(WHITE, 0, 0, render.width, render.height);
			clock.update(now, render, BLACK);
			weather.update(now, render, BLACK);
			render.end();
		}
	} else if (mode == Mode.Timer) {
		updateTimer(now);
	}
}, 100);

function updateTimer(now) {
	const text = "Timer!";

	render.begin();
	render.fillRectangle(WHITE, 0, 0, render.width, render.height);
	const textWidth = render.getTextWidth(text, TIME_FONT);
	const x = (render.width - textWidth) >> 1;
	const y = (render.height - TIME_FONT.height) >> 1;
	render.drawText(text, TIME_FONT, BLACK, x, y);
	render.end();
}
