import {Request} from "http"

const SECONDS = 1000;
const MINUTES = 60 * SECONDS;
const UPDATE_INTERVAL = 10 * SECONDS;

export default class Weather {
    constructor(settings, font) {
        this.font = font;

        this.appid = settings.appid;
        this.zipcode = settings.zip;
        this.country = settings.country;

        this.nextUpdate = Date.now();
        this.condition = "---";
        this.updated = false;
    }

    willUpdate(now) {
        if (now.getTime() > this.nextUpdate) {
    		this.nextUpdate = now.getTime() + UPDATE_INTERVAL;

    		let request = new Request({
    			host: "api.openweathermap.org",
    			path: `/data/2.5/weather?zip=${this.zipcode},${this.country}&appid=${this.appid}&units=imperial`,
    			response: String
    		});

    		request.callback = (message, value) => {
    			if (Request.responseComplete === message) {
    				this.updated = true;
    				value = JSON.parse(value, ["main", "name", "temp", "weather"]);
    				this.condition = `${parseInt(value.main.temp)}°F ${value.weather[0].main}`;
    			}
    		}
    	}

    	return this.updated;
    }

    update(now, screen, color) {
        this.updated = false;
    	const textWidth = screen.getTextWidth(this.condition, this.font);
    	const x = (screen.width - textWidth) >> 1;
        const y = screen.height - this.font.height - 10;
    	screen.drawText(this.condition, this.font, color, x, y);
    }
}
